<?php

/**
 * Created with love by ПИК Группа.
 * Author: Anochin Nikolay
 * Email: titan12345@mail.ru
 * Phone: +7 925 174 1314
 * Date: 07.03.2018
 * Time: 18:40
 */
namespace backend\components;

use yii\base\Component;
use common\models\TreeList as TreeListAR;
use yii\base\Exception;

/**
 * Первый вариант реализации задания
 * Class TreeList
 */
class TreeList extends Component
{
    /**
     * Загружаем данные в Базу
     * @param array $data
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    public function loadData(Array $data)
    {
        $transaction = \Yii::$app->db->beginTransaction();
        $result = true;

        foreach ($data as $item) {
            $treeList = new treeListAR();

            $positions = explode('.', $item['position']);

            // Записваем по левелам дерева
            $treeList->first_level = (!empty($positions[0])) ? $positions[0] : null;
            $treeList->second_level = (!empty($positions[1])) ? $positions[1] : null;
            $treeList->third_level = (!empty($positions[2])) ? $positions[2] : null;
            $treeList->fourth_level = (!empty($positions[3])) ? $positions[3] : null;
            $treeList->fifth_level = (!empty($positions[4])) ? $positions[4] : null;

            if (!$treeList->first_level &&
                !$treeList->second_level &&
                !$treeList->third_level &&
                !$treeList->fourth_level &&
                !$treeList->fifth_level
            ) {
                throw new Exception("Invalid data. Please check it!");
            }

            $treeList->title = $item['title'];

            // Забираем цену
            if (preg_match("/[0-9]+[.][0-9]{2}/ui", $item['price'], $match)) {
                $treeList->price = $match[0];
            } else {
                throw new Exception("Invalid data. Please check it!");
            }

            $result &= $treeList->save();
        }

        if ($result) {
            $transaction->commit();
        } else {
            $transaction->rollBack();
            throw new Exception("Invalid data. Please check it!");
        }

    }

    /**
     * Возвращаем данные ввиде дерева
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getTree()
    {
        $final_tree = [];
        $tree = treeListAR::find()
            ->orderBy('first_level, second_level, third_level, fourth_level, fifth_level')
            ->asArray()
            ->all();

        foreach ($tree as $item) {
            if (!$item['second_level'] && !$item['third_level'] && !$item['fourth_level'] && !$item['fifth_level']) {
                $final_tree[$item['first_level']] = [
                    'text' => $item['title'] . ' ' . $item['price'],
                    'nodes' => []
                ];
            } else if (!$item['third_level'] && !$item['fourth_level'] && !$item['fifth_level']) {
                $final_tree[$item['first_level']]['nodes'][$item['second_level']] = [
                    'text' => $item['title'] . ' ' . $item['price'],
                    'nodes' => []
                ];
            } else if (!$item['fourth_level'] && !$item['fifth_level']) {
                $final_tree[$item['first_level']]['nodes'][$item['second_level']]['nodes'][$item['third_level']] = [
                    'text' => $item['title'] . ' ' . $item['price'],
                    'nodes' => []
                ];
            } else if (!$item['fifth_level']) {
                $final_tree[$item['first_level']]['nodes'][$item['second_level']]['nodes'][$item['third_level']]['nodes'][$item['fourth_level']] = [
                    'text' => $item['title'] . ' ' . $item['price'],
                    'nodes' => []
                ];
            } else {
                $final_tree[$item['first_level']]['nodes'][$item['second_level']]['nodes'][$item['third_level']]['nodes'][$item['fourth_level']]['nodes'] = [
                    'text' => $item['title'] . ' ' . $item['price'],
                ];
            }
        }

        return $final_tree;
    }

}