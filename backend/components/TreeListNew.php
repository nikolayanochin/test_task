<?php
/**
 * Created with love by ПИК Группа.
 * Author: Anochin Nikolay
 * Email: titan12345@mail.ru
 * Phone: +7 925 174 1314
 * Date: 09.03.2018
 * Time: 13:14
 */


namespace backend\components;

use common\models\TreeListNew as TreeList;
use yii\base\Exception;
use yii\helpers\ArrayHelper;

/**
 * Второй вариант реализации задания
 * Class TreeListNew
 * @package backend\components
 */
class TreeListNew
{
    private $data = [];

    /**
     * Загружаем данные в Базу
     * @param array $data
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    public function loadData(Array $data)
    {
        foreach ($data as &$item) {
            $item['level'] = count(explode('.', $item['position']));
        }

        ArrayHelper::multisort($data, ['level', 'position']);
        $this->data = ArrayHelper::index($data, 'position');

        $transaction = \Yii::$app->db->beginTransaction();
        $result = true;

        foreach ($this->data as &$item) {
            if (!$result) {
                break;
            }

            // Ищем родителя, если его нет ищем родителя на уровень выше
            $parent_position = $this->getParent($item['position']);

            while (empty($this->data[$parent_position]) && $parent_position != 0) {
                $parent_position = $this->getParent($parent_position);
            }

            $parent_id = !empty($this->data[$parent_position]['id']) ? $this->data[$parent_position]['id'] : 0;

            $treeList = new TreeList();
            $treeList->title = $item['title'];
            $treeList->parent_id = $parent_id;
            $treeList->level = $item['level'];

            // Забираем цену
            if (preg_match("/[0-9]+[.][0-9]{2}/ui", $item['price'], $match)) {
                $treeList->price = $match[0];
            } else {
                throw new Exception("Invalid data. Please check it!");
            }

            $result &= $treeList->save();
            $item['id'] = $treeList->id;
        }

        if ($result) {
            $transaction->commit();
        } else {
            $transaction->rollBack();
            throw new Exception("Invalid data. Please check it!");
        }

    }

    /**
     * Полуаем предполагаемого
     * @param $position
     * @return int|string
     */
    private function getParent($position)
    {
        $positions = explode('.', $position);

        if (count($positions) == 1) {
            return 0;
        }

        $parent = $positions[0];

        for ($i = 0; $i < count($positions) - 2; $i++) {
            $parent .= '.' . $positions[$i + 1];
        }

        return $parent;
    }

    /**
     * Возвращаем данные ввиде дерева
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getTree()
    {
        $data = TreeList::find()->asArray()->orderBy('level DESC, parent_id DESC')->indexBy('id')->all();

        foreach ($data as &$item) {
            $item['text'] = $item['title'] . ' ' . $item['price'];
            if ($item['parent_id'] != 0) {
                $data[$item['parent_id']]['nodes'][] = $item;
                unset($data[$item['id']]);
            }
        }

        return $data;
    }
}