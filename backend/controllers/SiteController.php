<?php
namespace backend\controllers;

use common\models\TreeListNew;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $data = [
            [
                'position' => '3.1.2.4.1',
                'title' => 'title4',
                'price' => '13333392.00 R',
            ],
            [
                'position' => '3.1.2.4.5',
                'title' => 'title5',
                'price' => '13333392.00 R',
            ],
            [
                'position' => '3.1.2.4.2',
                'title' => 'title6',
                'price' => '13333392.00 R',
            ],
            [
                'position' => '3.1',
                'title' => 'title1',
                'price' => '192.02 R',
            ],
            [
                'position' => '3.1.2',
                'title' => 'title2',
                'price' => '192.00 R',
            ],
            [
                'position' => '3.1.2.4',
                'title' => 'title3',
                'price' => '192.00 R',
            ],
            [
                'position' => '4',
                'title' => 'title1a',
                'price' => '192.02 R',
            ],
            [
                'position' => '4.1.2',
                'title' => 'title2s',
                'price' => '192.00 R',
            ],
            [
                'position' => '4.1.2.4',
                'title' => 'title3g',
                'price' => '192.00 R',
            ],

        ];

        /** @var \backend\components\TreeListNew $treeListComponent */
        $treeListComponent = Yii::$app->get('treeListNew');

        TreeListNew::deleteAll();

        $treeListComponent->loadData($data);
        $tree = $treeListComponent->getTree();

        return $this->render('index', [
            'tree' => $tree
        ]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
