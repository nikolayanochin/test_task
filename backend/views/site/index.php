<?php
/* @var $this yii\web\View */
/* @var array $tree */

?>
<ul class="nav nav-list">
    <?php
    foreach ($tree as $item) {
        echo '<li><label class="tree-toggler nav-header">' . $item['text'] . '</label>';
        echo '<ul class="nav nav-list tree">';
        if (empty($item['nodes']) || !is_array($item['nodes'])) continue;
        foreach ($item['nodes'] as $item1) {
            echo '<li><a href="#">' . $item1['text'] . '</a>';
            echo '<ul class="nav nav-list tree">';
            if (empty($item1['nodes'])  || !is_array($item1['nodes'])) continue;
            foreach ($item1['nodes'] as $item2) {
                echo '<li><a href="#">---- ' . $item2['text'] . '</a>';
                echo '<ul class="nav nav-list tree">';
                if (empty($item2['nodes']) || !is_array($item2['nodes'])) continue;
                foreach ($item2['nodes'] as $item3) {
                    echo '<li><a href="#">-------- ' . $item3['text'] . '</a></li>';
                    echo '<ul class="nav nav-list tree">';
                    if (empty($item3['nodes']) || !is_array($item3['nodes'])) continue;
                    foreach ($item3['nodes'] as $item4) {
                        echo '<li><a href="#">---------------- ' . $item4['text'] . '</a></li>';
                    }
                    echo '</ul></li>';
                }
                echo '</ul></li>';
            }
            echo '</ul></li>';
        }
        echo '</ul>';
        echo '</li>';
    }
    ?>
</ul>
