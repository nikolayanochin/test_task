<?php

namespace common\models\base;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "tree_list".
 *
 * @property int $id Идентификатор
 * @property string $title Название
 * @property double $price Цена
 * @property int $first_level Позиция 1-го уровня
 * @property int $second_level Позиция 2-го уровня
 * @property int $third_level Позиция 3-го уровня
 * @property int $fourth_level Позиция 4-го уровня
 * @property int $fifth_level Позиция 5-го уровня
 * @property string $created_at
 */
class TreeListBase extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tree_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'price'], 'required'],
            [['price'], 'number'],
            [['first_level', 'second_level', 'third_level', 'fourth_level', 'fifth_level'], 'integer'],
            [['created_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Идентификатор',
            'title' => 'Название',
            'price' => 'Цена',
            'first_level' => 'Позиция 1-го уровня',
            'second_level' => 'Позиция 2-го уровня',
            'third_level' => 'Позиция 3-го уровня',
            'fourth_level' => 'Позиция 4-го уровня',
            'fifth_level' => 'Позиция 5-го уровня',
            'created_at' => 'Created At',
        ];
    }
}
