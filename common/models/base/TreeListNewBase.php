<?php

namespace common\models\base;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "tree_list_new".
 *
 * @property int $id Идентификатор
 * @property string $title Название
 * @property double $price Цена
 * @property int $parent_id Родитель
 * @property int $level Уровень
 */
class TreeListNewBase extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tree_list_new';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'price'], 'required'],
            [['price'], 'number'],
            [['parent_id', 'level'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Идентификатор',
            'title' => 'Название',
            'price' => 'Цена',
            'parent_id' => 'Родитель',
            'level' => 'Уровень',
        ];
    }
}
