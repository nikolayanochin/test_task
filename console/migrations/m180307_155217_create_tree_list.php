<?php

use yii\db\Migration;

/**
 * Class m180307_155217_create_tree_list
 */
class m180307_155217_create_tree_list extends Migration
{
    const TREE_LIST = '{{%tree_list}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(self::TREE_LIST, [
            'id' => $this->primaryKey()->comment('Идентификатор'),
            'title' => $this->string(255)->notNull()->comment('Название'),
            'price' => $this->double()->notNull()->comment('Цена'),
            'first_level' => $this->integer()->comment('Позиция 1-го уровня'),
            'second_level' => $this->integer()->comment('Позиция 2-го уровня'),
            'third_level' => $this->integer()->comment('Позиция 3-го уровня'),
            'fourth_level' => $this->integer()->comment('Позиция 4-го уровня'),
            'fifth_level' => $this->integer()->comment('Позиция 5-го уровня'),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::TREE_LIST);
    }
}
