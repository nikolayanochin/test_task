<?php

use yii\db\Migration;

/**
 * Class m180309_101634_create_tree_list_new
 */
class m180309_101634_create_tree_list_new extends Migration
{
    const TREE_LIST_NEW = '{{%tree_list_new}}';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(self::TREE_LIST_NEW, [
            'id' => $this->primaryKey()->comment('Идентификатор'),
            'title' => $this->string(255)->notNull()->comment('Название'),
            'price' => $this->double()->notNull()->comment('Цена'),
            'parent_id' => $this->integer()->notNull()->defaultValue(0)->comment('Родитель'),
            'level' => $this->integer()->notNull()->defaultValue(0)->comment('Уровень')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(self::TREE_LIST_NEW);
    }
}
